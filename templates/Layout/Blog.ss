<!--

    This template should include a list of all the blog posts created by the merchant.

    Note: Make sure to include pagination / infinite scroll here.

    Note: That a blog post might not always have an image, so image logic
    should take that into consideration. We recommend using the site logo
    if an image is not set. `$SiteSettings.Logo`
-->

<% loop $PaginatedPages(_limit_) %>
    <!-- Blog Post data available here -->
<% end_loop %>