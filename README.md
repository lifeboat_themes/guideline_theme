# Lifeboat Theme Development Guidelines

## General Requirements
- Theme should contain `config.json` file
- Theme should contain `VERSION` file  
- Theme version ping url, and update url should be online and live (not in beta or staging)  
- Theme should include full documentation in `README.md`
- A fully configured sample site using this theme should be created
- Documentation page created on lifeboat.app website

## Template List
- Page.ss
- Layout/Blog.ss
- Layout/BlogPage.ss
- Layout/Cart.ss
- Layout/Collection.ss
- Layout/Contact.ss
- Layout/Customers.ss
- Layout/CustomPage.ss
- Layout/IndexPage.ss
- Layout/PageNotFound.ss
- Layout/Product.ss
- Layout/Search.ss
- Layout/WishList.ss

## UX Tests
### Master Template (Page.ss)
- Should include `$SystemAdditions`
- Confirm all JS, CSS, fonts and other static files are included using `$StaticFile($file_path)`
- Should include MainMenu `$SiteSettings.MainMenu`
- Show a cart icon on every page (when clicked opens the cart or goes to `/cart`)
- Show a menu icon on every page (when clicked opens the menu)
- Show a login/profile icon on every page (when clicked goes to `/customer/profile`)

### Custom Fields
- Confirm all custom fields work as intended
- Confirm all custom fields are included in the theme documentation

### Pricing
- Prices should be formatted to use the selected currency
- Prices should always include Taxes
- Prices should be rounded to 2 decimal places

### Product Cards
- Should have an `Add to Cart` button (if has Variant, button should say `View`)
- Should display first Image (if not available, use logo)
- `Add to cart` should add 1 of the clicked items to the cart
- Should display `Out of Stock` if item is out of stock
- Should have option to `Add to Wishlist` & `Remove from Wishlist`
- Test all functionality with a product using variants, and one that doesn't

### Product Page
- Should display variants as dropdowns (if applicable)
- When variant options are selected, price should be updated automatically
- Only allow for valid variant combinations
- Should have an option to set quantity to add to cart
- When adding to cart, product should be added respecting selected variants and quantity
- Should display `Out of Stock` if item is out of stock
- Should display all the media, in the same order as it exists in the admin
- If video is available, it should be muted by default
- Page should include; `Title`, `Summary`, `Description`, `Additional Descriptions` & `Reviews`
- Optionally display: `Vendor` & `SKU`
- Tags should not be displayed
- Test all functionality with a product using variants, and one that doesn't

### Cart
- Should display all items currently in the cart, the selected variant, and quantity
- Should have option to remove item from cart
- Should show the cart Subtotal via: `$Cart.FormatPrice('Subtotal', 1)`
- Should not allow proceeding to checkout if `$Cart.canOrder()` is not true
- Show `$Cart.FormatPrice('MinOrder')` is `$Cart.canOrder()` is false to notify user they haven't reach the minimum order amount
- Optional: Have option to change quantity

### Blog
- Should include a list of all blog posts, paginated
- If a blog post doesn't have an image set, default to site logo

### Blog Page
- Should include the `$Content` of the blog post
- Should include breadcrumbs with links to `$Parent`  
- Optionally include 'Latest Posts', 'Similar Posts', etc.

### Collection
- Should display all the products in the collection
- Products should follow the same order as specified in the admin (unless sorting has been applied)
- Products should be paginated
- User should be able to filter by; `Price` & `Search Data`
- User should be able to sort the results
- Refer to `$getOptions()` for all available filtering and sorting options

### Contact
- User should be able to submit a contact form, sent to the site admin
- Should include Recaptcha: `$IncludeReCaptchaField('contact')`
- Should display: `$Email`, `$Address`, `$OpeningHours`

### Customers
- Should include `$Form`
- Should NOT try to alter form functionality
- Confirm `Login`, `Register`, `Forgot Password`, and `Verification` work

### CustomPage
- Should include the `$Content` of the page

### IndexPage
- Should include various `CustomFields` to allow a good balance between customisation and ease of use
- Page should still look relatively good even if no CustomFields have been configured

### Profile
- Should include `$ProfileDetailsForm` and confirm it works
- Should include `$ProfileAddressesForm` and confirm it works

### Search
- Should display a paginated list of results
- Should include all filtering and sorting options as the `Collection Page`
- Should include option to filter by `Collection`
- Should include option to search by `Search Term`

### WishList
- Should display a list of all items added to the wishlist
- Should have option to remove item from wishlist

### Social Links
- Should always open in a new tab